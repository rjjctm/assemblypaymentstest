import { Component, OnInit } from '@angular/core';
import { CatalogueService } from '../../services/catalogue.service';
import { MessageService } from '../../services/message.service';
import { Subscription } from 'rxjs';
import { CartItem } from '../../model/cart-item';

@Component({
  selector: 'app-store-catalogue',
  templateUrl: './store-catalogue.component.html',
  styleUrls: ['./store-catalogue.component.scss']
})
export class StoreCatalogueComponent implements OnInit {

  public catalogue:Array<Object> = [];
  public notification:string = "";
  private sub: Subscription;

  constructor(private _catalogueService: CatalogueService, private _messageService: MessageService) {
    this.sub = this._catalogueService.loadProducts()
    .subscribe(
      (data)=>{
        this.catalogue = data['cards'];

        //We are using the hp attribute for each card as the cost. 
        //Not all cards have hp so need to put in default of 5 dollars
        this.catalogue.forEach(item=>{
          const itemCost = parseInt(item['hp']);
          if(!Number.isInteger(itemCost)){
            item['hp'] = 5;
          } else {
            item['hp'] = itemCost;
          }
        })         
      },
      error=>{
        this.notification = "Cannot retrieve catalogue please try again later";
      }
    )
  }

  addToCart(name:string, cost:number){
    let selectedItem:CartItem = {name:name, cost:cost, quantity:1}
    this._messageService.sendMessage(selectedItem);
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
