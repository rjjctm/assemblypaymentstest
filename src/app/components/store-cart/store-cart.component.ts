import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../services/message.service';
import { CartService } from '../../services/cart.service';
import { Subscription } from 'rxjs';
import { CartItem } from '../../model/cart-item';
import { Transaction } from '../../model/transaction';

@Component({
  selector: 'app-store-cart',
  templateUrl: './store-cart.component.html',
  styleUrls: ['./store-cart.component.scss']
})
export class StoreCartComponent implements OnInit {

  public total: number = 0;
  public cartItems: Array<CartItem> = [];
  public result:string = "";
  public totalItemNum:number = 0;
  private sub: Subscription;
  private transaction: Subscription;
  
  constructor(private _messageService: MessageService, private _cartService: CartService) {

    this.sub = this._messageService.getMessage()
    .subscribe((cartItem)=>{
      this.addToCart(cartItem);
    })
  }

  addToCart(cartItem:CartItem) {
    const name = cartItem.name;
    const carItemsLength = this.cartItems.length;

    if(carItemsLength == 0){
      this.cartItems.push(cartItem)
    } else {
      for(var i=0; i < carItemsLength; i++){          
        if(this.cartItems[i].name == name) {
          this.cartItems[i].quantity ++;
          break;
        } else if(i == carItemsLength - 1) {
          this.cartItems.push(cartItem)
        }
      }
    }
    this.total += cartItem.cost;
    this.totalItemNum += 1;
    this.result = '';
  }

  removeFromCart(cost:number, i:number) {
    if(this.cartItems[i].quantity == 1) {
      this.cartItems.splice(i,1);
    } else {
      this.cartItems[i].quantity --;
    }
    this.total -= cost;
    this.totalItemNum -= 1;
    this.result = '';
  }

  addToCartFromCart(cost:number, i:number){
    this.cartItems[i].quantity ++;
    this.total += cost;
    this.totalItemNum += 1;
    this.result = '';
  }

  sendTransaction(){
    if(this.total == 0) {
      this.result = "You shopping cart is empty! Please add some items"
    } else {
      this.transaction = this._cartService.sendTransaction(this.total)
      .subscribe(
        (data:Transaction)=>{
          this.cartItems = [];
          this.total = 0;
          const reference = Math.random().toString().substring(2);
          this.result = `$${data['amount']} was paid successfully! 
          Please keep your receipt number for future reference: #${reference}`;
          this.totalItemNum = 0;        
        },
        (error) =>{
          this.result = "Payment was unsuccessful please try again later"
        }
      )
    }
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.transaction.unsubscribe();
  }
}
