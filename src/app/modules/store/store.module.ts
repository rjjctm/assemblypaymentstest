import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { StoreCatalogueComponent } from '../../components/store-catalogue/store-catalogue.component';
import { StoreCartComponent } from '../../components/store-cart/store-cart.component';

import { CatalogueService } from '../../services/catalogue.service';
import { MessageService } from '../../services/message.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    StoreCatalogueComponent,
    StoreCartComponent
  ],
  declarations: [
    StoreCatalogueComponent,
    StoreCartComponent
  ],
  providers: [
    CatalogueService,
    MessageService
  ]
})
export class StoreModule { }
