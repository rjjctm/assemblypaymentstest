export interface CartItem {
    name:string,
    quantity:number,
    cost:number,
}
