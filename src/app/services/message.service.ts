import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CartItem } from '../model/cart-item';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private subject = new Subject<any>();

  constructor() { }

  sendMessage(message:CartItem){
    this.subject.next(message)
  }

  getMessage(): Subject<CartItem> {
    return this.subject;
  }
}
