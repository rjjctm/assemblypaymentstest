import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { webSocket } from 'rxjs/webSocket';
import { Transaction } from '../model/transaction';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private subject:Subject<Transaction>;
  
  constructor() { }

  public sendTransaction(total) {
    let transaction = {event:'purchase', amount:total} 
    this.subject = webSocket('ws://demos.kaazing.com/echo')
    this.subject.next(transaction)    

    return this.subject;
  }
}
