import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {

  constructor(private http:HttpClient) { }

  public loadProducts(): Observable<object> {
    return this.http.get('https://api.pokemontcg.io/v1/cards?pageSize=15')
  }
}
