import { TestBed } from '@angular/core/testing';

import { CatalogueService } from './catalogue.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('CatalogueService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers:[HttpClient, HttpHandler]
  }));

  it('should be created', () => {
    const service: CatalogueService = TestBed.get(CatalogueService);
    expect(service).toBeTruthy();
  });
});
