# Assemblypaymentstest
Jack Chen 2018

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Application Design

### Modules
1. store module - packages up store-cart and store-catalogue components and related services

### Components
1. store-cart component - contains list of items added the shopping cart as well as the payment button
2. store-catalogue component - retireve a list of dummy catalogue from an real api

### Services
1. cart service - service use by store-cart component for making payment
2. catalogue service - service use by store-catalogue for loading product catalogue
3. message service - service use by both components above to trigger adding items to the store-cart component from the store-catalogue component

### Models
1. cartItem interface - trying to standardise the message which gets passed around between the 2 main components.

### Notes
1. The catalogue api can sometime be slow "https://api.pokemontcg.io/v1/cards?pageSize=15" I think its because they might have some rate limiter on it. 
2. For pricing I have to use the "hp" attribute in the api as its the closest thing which returns mostly numbers but not all items have that attribute populated so I have a bit of the code which make all non numeric values to 5.
3. I wasn't sure how much details I should go in terms of styling and functionality. For example I would like to add pagination to the product catalogue and maybe have different catagories too but wasn't sure if its something you guys want to see as part of the deliverables so gonna cut those out for time sake.